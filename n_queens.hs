-- import System.IO
-- move like rook
torre::Int->(Int,Int)->[(Int,Int)]
torre n (x,y) = horizontal ++ vertical
              where horizontal = [(a,y) | a <- [1..n], a /= x]
                    vertical   = [(x,b) | b <- [1..n], b /= y]
-- move like bishop                    
bispo::Int->(Int,Int)->[(Int,Int)]            
bispo n (x,y) = [(a,b) | a <- [1..n], b <- [1..n], abs(a-x) == abs(b-y), abs(a-x) > 0 , abs(b-y) > 0]
-- foi substituido pois ficaria alocando memoria desnecessario
-- tabuleiro n = fromList n n [1..]
-- foi substituido pois ficaria alocando memoria desnecessario
-- pegaTabuleiro n (x,y) = getElem x y (tabuleiro n)
pegaTabuleiro n (x,y) = (x - 1) * n + y
-- movimentos da rainha
queen n = [torre n (a,b) ++ bispo n (a,b) | a <- [1..n], b <- [1..n]]
-- suz ela retorna a lista com os valores da matriz
valoracoes n a = [produto (subLista n x) | x <- a]
--  subLista ela retorna o numero da matriz
subLista n l = [pegaTabuleiro n x | x <- l] 
-- produto essa função nega os valores
produto l = [x*(-1) | x <- l]

-- lista para o arquivo
listArq n l = [subListaArq n x | x <- l]
-- subListaArq para escrever no arquivo
subListaArq n l = [escrever x | x <- l]
escrever [] = do
                putStrLn "Escrita realizada com sucesso!"
escrever (a:as) = do
                appendFile "teste.txt" (show a ++ "\n")
                putStrLn "Escrita realizada com sucesso!"
                escrever as


ler :: IO ()
ler = do
        conteudo <- readFile "teste.txt"
        putStrLn conteudo


anexar = do
            appendFile "teste.txt" "\nHaskell eh legal"
            putStrLn "Conteudo anexado com sucesso"
